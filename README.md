# show_tz.java

Informa timezone da JVM e se está no horário de verão.  



## Execução sem compilação
```bash
java show_tz.java
```



## Execução como script java
```bash
{ echo '#!/usr/bin/env -S java --source 19'; cat show_tz.java; } > show_tz
chmod +x ./show_tz
./show_tz
```



## Execução da classe compilada
```bash
javac show_tz.java
java show_tz
```

Compilação cruzada compatível com versões muito antigas do java não é mais suportada pelas versões mais atuais.  
Será preciso instalar um java 8, por exemplo, para compatibilidade a partir da versão 1.6:
```bash
 jvers=8
 jhome=/usr/lib/jvm/java-$jvers-openjdk-amd64
compat=1.6

sudo apt install -y openjdk-$jvers-jdk

$jhome/bin/javac show_tz.java -bootclasspath $jhome/jre/lib/rt.jar -source $compat -target $compat
```



## Uso
```bash
java [-Duser.timezone=<tz>] show_tz [yyyy/mm/dd [hh:mm]]
```



## Exemplos
```shell-session
$ java -version
openjdk version "16.0.1" 2021-04-20
OpenJDK Runtime Environment (build 16.0.1+9-Ubuntu-120.04)
OpenJDK 64-Bit Server VM (build 16.0.1+9-Ubuntu-120.04, mixed mode, sharing)

$ /usr/lib/jvm/java-8-openjdk-amd64/bin/java -version
openjdk version "1.8.0_292"
OpenJDK Runtime Environment (build 1.8.0_292-8u292-b10-0ubuntu1~20.04-b10)
OpenJDK 64-Bit Server VM (build 25.292-b10, mixed mode)

$ java show_tz
Versão do Java     : 16.0.1
Class file         : /export/src/git/gitlab.com/show_tz.java/show_tz.class
Compatibilidade    : 50 (Java SE 6)
/etc/localtime     : /usr/share/zoneinfo/America/Sao_Paulo
Data e hora no SO  : 2021/06/07 Mon 18:31:14 -0300
Data e hora na JVM : 2021/06/07 Mon 18:31:14 -0300
Fuso horário       : Horário Padrão de Brasília
Horário de verão   : false

$ java show_tz 2020/01/10
Versão do Java     : 16.0.1
Class file         : /export/src/git/gitlab.com/show_tz.java/show_tz.class
Compatibilidade    : 50 (Java SE 6)
/etc/localtime     : /usr/share/zoneinfo/America/Sao_Paulo
Data e hora no SO  : 2020/01/10 Fri 18:35:00 -0300
Data e hora na JVM : 2020/01/10 Fri 18:35:00 -0300
Fuso horário       : Horário Padrão de Brasília
Horário de verão   : false

$ java show_tz 2019/01/10
Versão do Java     : 16.0.1
Class file         : /export/src/git/gitlab.com/show_tz.java/show_tz.class
Compatibilidade    : 50 (Java SE 6)
/etc/localtime     : /usr/share/zoneinfo/America/Sao_Paulo
Data e hora no SO  : 2019/01/10 Thu 18:35:00 -0200
Data e hora na JVM : 2019/01/10 Thu 18:35:00 -0200
Fuso horário       : Horário Padrão de Brasília
Horário de verão   : true

$ /usr/lib/jvm/java-8-openjdk-amd64/bin/java -Duser.timezone=GMT-3 show_tz
Versão do Java     : 1.8.0_292
Class file         : /export/src/git/gitlab.com/show_tz.java/show_tz.class
Compatibilidade    : 50 (Java SE 6)
/etc/localtime     : /usr/share/zoneinfo/America/Sao_Paulo
Data e hora no SO  : 2021/06/07 Mon 18:31:40 -0300
Data e hora na JVM : 2021/06/07 Mon 18:31:40 -0300
Fuso horário       : GMT-03:00
Horário de verão   : false

$ /usr/lib/jvm/java-8-openjdk-amd64/bin/java -Duser.timezone=GMT-3 show_tz 2019/01/10
Versão do Java     : 1.8.0_292
Class file         : /export/src/git/gitlab.com/show_tz.java/show_tz.class
Compatibilidade    : 50 (Java SE 6)
/etc/localtime     : /usr/share/zoneinfo/America/Sao_Paulo
Data e hora no SO  : 2019/01/10 Thu 19:37:00 -0200
Data e hora na JVM : 2019/01/10 Thu 18:37:00 -0300
Fuso horário       : GMT-03:00
Horário de verão   : false
```



## Referências
- [Depreciação dos construtores públicos da classe Locale a no Java 19](http://cursos.alura.com.br/forum/topico-locale-aparece-como-metodo-depreciado-266350)
- [Shebang para execução de scripts java a partir do Java 11](http://nipafx.dev/scripting-java-shebang)



☐
