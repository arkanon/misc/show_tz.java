#!/usr/bin/env -S java --source 19
/*

   show_tz.java

   Informa timezone da JVM e se está no horário de verão

   Arkanon <arkanon@lsd.org.br>
   2024/07/11 (Thu) 15:41:58 -03
   2021/06/08 (Tue) 03:34:00 -03
   2020/11/25 (Wed) 18:44:23 -03
   2020/10/30 (Fri) 16:28:43 -03

   Guilherme Becki <beckiguilherme@gmail.com>
   2019

*/

   import java.text.*;
   import java.util.*;
   import java.sql.Time;
   import java.io.*;

   public class show_tz
   {

     public static String shell_exec(String[] cmd)
     {
       String o = "";
       try
       {
         Process        p = Runtime.getRuntime().exec(cmd);
         BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
         String r;
         while ( ( r = b.readLine() ) != null ) o+=r;
       }
       catch (Exception e) { o = "error"; }
       return o;
     }

     public static void main(String[] args)
     {

       show_tz    main        = new show_tz();

       String     className   = main.getClass().getSimpleName();
       String     jVersion    = System.getProperty("java.version");
       Calendar   now         = Calendar.getInstance();

       TimeZone   timezone    = now.getTimeZone();
    // TimeZone   timezone    = TimeZone.getTimeZone("GMT-3");
    // TimeZone   timezone    = TimeZone.getTimeZone("America/Sao_Paulo");
    // timezone.setDefault(timezone);

       File       link        = new File("/etc/localtime");
       String     target      = "";

       try
       {
         target = link.getCanonicalPath();
       }
       catch (IOException e)
       {
         System.err.println(e);
       }

    // Locale     locale      = new Locale("en", "US"); // java <  19
       Locale     locale      = Locale.of("en", "US");  // java >= 19

       DateFormat oDateFormat = new SimpleDateFormat("yyyy/MM/dd EEE HH:mm:ss Z", locale);
       DateFormat iDateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
       DateFormat iTimeFormat = new SimpleDateFormat("HH:mm:ss");
       Date       jDate       = new Date();

       try
       {
         if (args.length==1) jDate = iDateFormat.parse( args[0] + " " + iTimeFormat.format(jDate) );
         if (args.length==2) jDate = iDateFormat.parse( args[0] + " " + args[1] );
       }
       catch(java.text.ParseException e)
       {
         e.printStackTrace();
       }

       String path = main.getClass().getProtectionDomain().getCodeSource().getLocation().getPath();
       File   file = new File(path);
       if (file.isDirectory()) path += className + ".class";

       String osDate      = shell_exec( new String[] { "bash", "-c", "LC_ALL=C date -d '" + jDate + "' +'%Y/%m/%d %a %H:%M:%S %z'" } );
    // String cVersion    = Runtime.class.getPackage().getSpecificationVersion();
       int    compatMinor = 0;
       int    compatMajor = 0;

       try
       {
         InputStream inputstream = new FileInputStream(path);
         try
         {
           DataInputStream input = new DataInputStream(inputstream);
           input.skipBytes(4);
           compatMinor = input.readUnsignedShort();
           compatMajor = input.readUnsignedShort();
         }
         catch (IOException e)
         {
           System.err.println(e);
         }
       }
       catch (FileNotFoundException e)
       {
         System.err.println(e);
       }

       System.out.println( );
       System.out.println( "Versão do Java     : " + jVersion );
       System.out.println( "Class file         : " + path );
       System.out.println( "Compatibilidade    : " + compatMajor + " (Java SE " + (compatMajor-44) + ")");
       System.out.println( );
       System.out.println( link.getPath() +        "     : " + target );
       System.out.println( "Data e hora no SO  : " + osDate );
       System.out.println( "Data e hora na JVM : " + oDateFormat.format(jDate) );
       System.out.println( "Fuso horário       : " + timezone.getDisplayName() );
       System.out.println( "Horário de verão   : " + timezone.inDaylightTime(jDate) );
       System.out.println( );
       System.out.println( "Uso: java [-Duser.timezone=<tz>] " + className + " [yyyy/mm/dd [hh:mm]]" );
       System.out.println( );

     }
   }

// EOF
